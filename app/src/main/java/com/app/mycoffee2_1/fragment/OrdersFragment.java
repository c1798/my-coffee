package com.app.mycoffee2_1.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mycoffee2_1.R;
import com.app.mycoffee2_1.adapter.OrdersAdapter;
import com.app.mycoffee2_1.api.Api;
import com.app.mycoffee2_1.api.ApiConfig;
import com.app.mycoffee2_1.api.CoffeeCallback;
import com.app.mycoffee2_1.manager.LoginInfoManager;
import com.app.mycoffee2_1.model.Order;
import com.app.mycoffee2_1.model.Product;
import com.app.mycoffee2_1.model.Result;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class OrdersFragment extends BaseFragment {
    private final List<Order> orders = new ArrayList<>();
    private TextView title;
    private RecyclerView recyclerView;
    OrdersAdapter ordersAdapter = null;
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                ordersAdapter.setOrders(orders);
                ordersAdapter.notifyDataSetChanged();
            }
        }
    };
    public OrdersFragment() {
    }

    public static OrdersFragment newInstance() {
        return new OrdersFragment();
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_order;
    }

    @Override
    protected void initView() {
        title = mRootView.findViewById(R.id.title);
        recyclerView = mRootView.findViewById(R.id.orders_lv);
        title.setText("订单");
    }

    @Override
    protected void initData() {
        // set layout manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        ordersAdapter = new OrdersAdapter(getActivity(),
                new OrdersAdapter.OnItemBtnClickListener() {
                    @Override
                    public void onConfirmBtnClick(int position) {
                        Toast.makeText(getActivity(), "确认收货", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onBuyAgainBtnClick(int position) {
                        Toast.makeText(getActivity(), "再来一单", Toast.LENGTH_SHORT).show();
                    }
                });
        recyclerView.setAdapter(ordersAdapter);

        HashMap<String, Object> params = new HashMap<>();
        params.put("userId", LoginInfoManager.getInstance().getUserId(Objects.requireNonNull(getActivity())));
        getAllOrders(getActivity(), params);
    }
    public void getAllOrders(Context context, HashMap<String, Object> params) {
        Api.config(ApiConfig.ORDERS_LIST_BY_USER, params).getRequest(context, new CoffeeCallback() {
            @Override
            public void onSuccess(String res) {
                Result<List<Order>> result = new Gson().fromJson(res, new TypeToken<Result<List<Order>>>(){}.getType());
                if (result.getResultCode()!=null&&result.getResultCode()==200){
                    orders.removeAll(orders);
                    orders.addAll(result.getData());
                    mHandler.sendEmptyMessage(1);
                }
                System.out.println(result.getMessage());
            }

            @Override
            public void onFailure(Exception e) {
                System.out.println(e);
            }
        });
    }

}
