package com.app.mycoffee2_1.fragment;

import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mycoffee2_1.R;
import com.app.mycoffee2_1.api.Api;
import com.app.mycoffee2_1.api.ApiConfig;
import com.app.mycoffee2_1.api.CoffeeCallback;
import com.app.mycoffee2_1.manager.LoginInfoManager;
import com.app.mycoffee2_1.model.Result;
import com.app.mycoffee2_1.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MineFragment extends BaseFragment implements View.OnClickListener{
    private TextView title;
    private EditText et_nickname;
    private EditText et_phone;
    private EditText et_address;
    private Button bt_save;
    private Button bt_login_out;

    public MineFragment() {
    }

    public static MineFragment newInstance() {
        return new MineFragment();
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void initView() {
        title = mRootView.findViewById(R.id.title);
        et_nickname = mRootView.findViewById(R.id.nick_et);
        et_phone = mRootView.findViewById(R.id.phone_et);
        et_address = mRootView.findViewById(R.id.address_et);
        bt_save = mRootView.findViewById(R.id.save_btn);
        bt_login_out = mRootView.findViewById(R.id.login_out_btn);
    }

    @Override
    protected void initData() {
        bt_save.setOnClickListener(this);
        bt_login_out.setOnClickListener(this);
        title.setText("我的");
        User user = LoginInfoManager.getInstance().getUserInfo(getContext());
        if (user.getUserId()!=0){
            et_nickname.setText(user.getNickname());
            et_phone.setText(user.getPhone());
            et_address.setText(user.getAddress());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.save_btn:
                User user = LoginInfoManager.getInstance().getUserInfo(getContext());
                String nickname = et_nickname.getText().toString();
                String phone = et_phone.getText().toString();
                String address = et_address.getText().toString();
                FormBody formBody = new FormBody.Builder()
                        .add("userId", user.getUserId().toString())
                        .add("username", user.getUsername())
                        .add("password", user.getPasswordM5())
                        .add("nickname", nickname)
                        .add("address", address)
                        .add("phone", phone)
                        .build();
                update(formBody);
                break;
            case R.id.login_out_btn:
                LoginInfoManager.getInstance().clear(getContext());
                showToast("退出系统");
                getActivity().onBackPressed();
                break;
        }
    }
    private void update(FormBody formBody){
        Api.config(ApiConfig.USER_UPDATE, formBody).postRequest(new CoffeeCallback() {
            @Override
            public void onSuccess(String res) {
                Gson gson = new Gson();
                Result<User> result = gson.fromJson(res, new TypeToken<Result<User>>(){}.getType());
                if (result.getResultCode()==200){
                    LoginInfoManager.getInstance().saveUserInfo(getContext(), result.getData());
                }
                System.out.println(result.getMessage());
            }

            @Override
            public void onFailure(Exception e) {
                System.out.println(e);
                showToastSync("请求失败");
            }
        });
    }

}
