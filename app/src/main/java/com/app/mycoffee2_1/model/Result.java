package com.app.mycoffee2_1.model;

import java.security.Timestamp;

import lombok.Data;

@Data
public class Result<T> {
    private Integer resultCode;
    private String message;
    private T data;
    private Integer status;
    private String timestamp;
    private String error;
    private String path;
}
