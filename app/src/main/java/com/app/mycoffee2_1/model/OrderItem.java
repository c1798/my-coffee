package com.app.mycoffee2_1.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderItem {
    private Long orderId;
    private Long goodsId;
    private String goodsName;
    private BigDecimal sellingPrice;
    private Integer goodsCount;
}
