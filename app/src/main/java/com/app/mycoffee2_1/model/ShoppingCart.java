package com.app.mycoffee2_1.model;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShoppingCart {
    private Long id;
    private Long userId;
    private Long productId;
    private Integer num;
    private BigDecimal productPrice;
    private Boolean isSelected;
    private String productName;
    private String productDesc;
}
