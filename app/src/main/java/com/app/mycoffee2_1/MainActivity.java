package com.app.mycoffee2_1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.app.mycoffee2_1.adapter.CoffeePagerAdapter;
import com.app.mycoffee2_1.fragment.MineFragment;
import com.app.mycoffee2_1.fragment.OrdersFragment;
import com.app.mycoffee2_1.fragment.ProductFragment;
import com.app.mycoffee2_1.fragment.ShoppingCartFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout productLL;
    private LinearLayout orderLL;
    private LinearLayout shoppingCartLL;
    private LinearLayout mineLL;
    private ViewPager mViewPager;
    List<Fragment> mFragmentList = new ArrayList<Fragment>();
    private CoffeePagerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // 1.获取管理器
        FragmentManager mFragmentManager = getSupportFragmentManager();
        // 2.初始化对象
        initFragmentList();
        // 3.适配器
        mAdapter = new CoffeePagerAdapter(mFragmentManager, 0, mFragmentList);
        // 4.初始化界面
        initView();
        // 5.初始化viewPager
        initViewPager();
    }

    private void initFragmentList() {
        Fragment productFragment = ProductFragment.newInstance();
        Fragment shoppingCartFragment = ShoppingCartFragment.newInstance();
        Fragment orderFragment = OrdersFragment.newInstance();
        Fragment mineFragment = MineFragment.newInstance();
        mFragmentList.add(productFragment);
        mFragmentList.add(orderFragment);
        mFragmentList.add(shoppingCartFragment);
        mFragmentList.add(mineFragment);
    }

    private void initView() {
        productLL = findViewById(R.id.productLL);
        orderLL = findViewById(R.id.orderLL);
        shoppingCartLL = findViewById(R.id.shoppingCartLL);
        mineLL = findViewById(R.id.mineLL);
        mViewPager = findViewById(R.id.viewpaper);
        productLL.setOnClickListener(this);
        orderLL.setOnClickListener(this);
        shoppingCartLL.setOnClickListener(this);
        mineLL.setOnClickListener(this);
    }

    private void initViewPager() {
        // 1.设置监听
        mViewPager.addOnPageChangeListener(new ViewPagerOnPageChangeListener());
        // 2.设置适配器
        mViewPager.setAdapter(mAdapter);
        // 3.第一次加载时
        mViewPager.setCurrentItem(0);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.productLL:
//                Toast.makeText(this,"点击了商品",Toast.LENGTH_SHORT).show();
                mViewPager.setCurrentItem(0);
                updateBottomLinerLayoutSelect(true, false, false, false);
                break;
            case R.id.orderLL:
//                Toast.makeText(this,"点击了订单",Toast.LENGTH_SHORT).show();
                mViewPager.setCurrentItem(1);
                updateBottomLinerLayoutSelect(false, true, false, false);
                break;
            case R.id.shoppingCartLL:
//                Toast.makeText(this,"点击了购物车",Toast.LENGTH_SHORT).show();
                mViewPager.setCurrentItem(2);
                updateBottomLinerLayoutSelect(false, false, true, false);
                break;
            case R.id.mineLL:
//                Toast.makeText(this,"点击了我的",Toast.LENGTH_SHORT).show();
                mViewPager.setCurrentItem(3);
                updateBottomLinerLayoutSelect(false, false, false, true);
                break;
        }

    }

    class ViewPagerOnPageChangeListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
//            Toast.makeText(getApplicationContext(), "滑动"+(position+1)+"", Toast.LENGTH_SHORT).show();
            boolean[] state = new boolean[mFragmentList.size()];
            state[position] = true;
            updateBottomLinerLayoutSelect(state[0], state[1], state[2], state[3]);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    private void updateBottomLinerLayoutSelect(boolean p, boolean o, boolean s, boolean m) {
        productLL.setSelected(p);
        orderLL.setSelected(o);
        shoppingCartLL.setSelected(s);
        mineLL.setSelected(m);
    }
}