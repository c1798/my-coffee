package com.app.mycoffee2_1.manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.mycoffee2_1.model.User;

public class LoginInfoManager {
    private static LoginInfoManager instance;

    public LoginInfoManager() {
    }
    public static LoginInfoManager getInstance(){
        if (instance == null){
            instance = new LoginInfoManager();
        }
        return instance;
    }

    public void saveUserInfo(Context context, User loginUser){
        SharedPreferences sp = context.getSharedPreferences("sp_user", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong("USERID", loginUser.getUserId());
        editor.putString("USERNAME", loginUser.getUsername());
        editor.putString("PASSWORD", loginUser.getPasswordM5());
        editor.putString("NICKNAME", loginUser.getNickname());
        editor.putString("PHONE", loginUser.getPhone());
        editor.putString("ADDRESS", loginUser.getAddress());
        editor.apply();
    }

    public User getUserInfo(Context context){
        SharedPreferences sp = context.getSharedPreferences("sp_user", Context.MODE_PRIVATE);
        User user = new User();
        user.setUserId(sp.getLong("USERID", 0));
        user.setUsername(sp.getString("USERNAME", ""));
        user.setPasswordM5(sp.getString("PASSWORD", ""));
        user.setNickname(sp.getString("NICKNAME", ""));
        user.setPhone(sp.getString("PHONE", ""));
        user.setAddress(sp.getString("ADDRESS", ""));
        return user;
    }
    public Long getUserId(Context context){
        SharedPreferences sp = context.getSharedPreferences("sp_user", Context.MODE_PRIVATE);
        Long userId = sp.getLong("USERID", 0);
        return userId;
    }


    public void clear(Context context){
        SharedPreferences sp = context.getSharedPreferences("sp_user", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
    }
}
