package com.app.mycoffee2_1.manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.mycoffee2_1.model.ShoppingCart;
import com.app.mycoffee2_1.model.User;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartManager {
    private static ShoppingCartManager instance;
    private static String spFile = "sp_shopping";

    public ShoppingCartManager() {
    }
    public static ShoppingCartManager getInstance(){
        if (instance == null){
            instance = new ShoppingCartManager();
        }
        return instance;
    }

    public void saveShoppingCart(Context context, List<ShoppingCart> shoppingCarts){
        SharedPreferences sp = context.getSharedPreferences(spFile, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.apply();
    }

    public List<ShoppingCart> getShoppingCart(Context context){
        SharedPreferences sp = context.getSharedPreferences(spFile, Context.MODE_PRIVATE);
        List<ShoppingCart> shoppingCart = new ArrayList<>();

        return shoppingCart;
    }

    public void clear(Context context){
        SharedPreferences sp = context.getSharedPreferences(spFile, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
    }
}
