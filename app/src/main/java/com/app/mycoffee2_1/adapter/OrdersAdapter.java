package com.app.mycoffee2_1.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mycoffee2_1.R;
import com.app.mycoffee2_1.model.Order;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int resourceId;
    private List<Order> orders = null;
    private Context context = null;
    private OnItemBtnClickListener mListener;

    public OrdersAdapter(Context context, OnItemBtnClickListener listener) {
        this.context = context;
        this.mListener = listener;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_orders, parent, false);
        return new OrdersViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        Order order  =orders.get(position);
        OrdersViewHolder ovh = (OrdersViewHolder) holder;

        ovh.orderCodeTv.setText("订单编号：\n"+order.getOrderNo());
        // 订单状态:0.待支付 1.已支付 2.配货完成 3:出库成功 4.交易成功 -1.手动关闭 -2.超时关闭 -3.商家关闭
        String status = "待送达";
        switch (order.getOrderStatus()){
            case 0: status = "待支付"; break;
            case 1: status = "已支付"; break;
            case 2: status = "出单成功"; break;
            case 3: status = "制作完成"; break;
            case 4: status = "交易成功"; break;
            case -1: status = "手动关闭"; break;
            case -2: status = "超时关闭"; break;
            case -3: status = "商家关闭"; break;
        }
        if (order.getOrderStatus()>=4){
            ovh.confirmBtn.setText("再来一单");
        }
        ovh.statusTv.setText(status);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context.getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        ovh.ordersLv.setLayoutManager(linearLayoutManager);
        OrderAdapter orderAdapter = new OrderAdapter(context);
        orderAdapter.setOrderItem(order.getItemVOList());
        ovh.ordersLv.setAdapter(orderAdapter);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ovh.createTimeTv.setText(format.format(order.getCreateTime()));
        ovh.confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("确认收货".equals(ovh.confirmBtn.getText())){
                    mListener.onConfirmBtnClick(position);
                }else if ("再来一单".equals(ovh.confirmBtn.getText())){
                    mListener.onBuyAgainBtnClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (orders != null && orders.size() > 0) {
            return orders.size();
        } else {
            return 0;
        }
    }

    public interface OnItemBtnClickListener{

        void onConfirmBtnClick(int position);

        void onBuyAgainBtnClick(int position);
    }

    private class OrdersViewHolder extends RecyclerView.ViewHolder {
        TextView orderCodeTv;
        TextView statusTv;
        RecyclerView ordersLv;
        TextView createTimeTv;
        Button confirmBtn;
        public OrdersViewHolder(View view) {
            super(view);
            orderCodeTv = view.findViewById(R.id.order_code_tv);
            statusTv = view.findViewById(R.id.status_tv);
            ordersLv = view.findViewById(R.id.orders_lv);
            createTimeTv = view.findViewById(R.id.create_time_tv);
            confirmBtn = view.findViewById(R.id.confirm_btn);
        }
    }
    private class OrderViewHolder extends RecyclerView.ViewHolder {
        TextView productNameTv;
        TextView orderNumTv;
        TextView sumPriceTv;
        public OrderViewHolder(View view) {
            super(view);
            productNameTv = view.findViewById(R.id.product_name_tv);
            orderNumTv = view.findViewById(R.id.order_num_tv);
            sumPriceTv = view.findViewById(R.id.sum_price_tv);
        }
    }
}

