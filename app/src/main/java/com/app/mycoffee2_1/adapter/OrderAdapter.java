package com.app.mycoffee2_1.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mycoffee2_1.R;
import com.app.mycoffee2_1.model.OrderItem;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int resourceId;
    private List<OrderItem> orderItem = null;
    private Context context = null;

    public OrderAdapter(Context context) {
        this.context = context;
    }

    public void setOrderItem(List<OrderItem> orderItem) {
        this.orderItem = orderItem;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order, parent, false);
        return new OrderViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        OrderItem orderItem  = this.orderItem.get(position);
        OrderViewHolder ovh = (OrderViewHolder) holder;

        ovh.productNameTv.setText(orderItem.getGoodsName());
        ovh.orderNumTv.setText("数量"+orderItem.getGoodsCount());
        ovh.sumPriceTv.setText("￥"+orderItem.getSellingPrice());
    }

    @Override
    public int getItemCount() {
        if (orderItem != null && orderItem.size() > 0) {
            return orderItem.size();
        } else {
            return 0;
        }
    }

    private class OrderViewHolder extends RecyclerView.ViewHolder {
        TextView productNameTv;
        TextView orderNumTv;
        TextView sumPriceTv;
        public OrderViewHolder(View view) {
            super(view);
            productNameTv = view.findViewById(R.id.product_name_tv);
            orderNumTv = view.findViewById(R.id.order_num_tv);
            sumPriceTv = view.findViewById(R.id.sum_price_tv);
        }
    }
}

