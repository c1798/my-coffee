package com.app.mycoffee2_1.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mycoffee2_1.model.Product;
import com.app.mycoffee2_1.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Product> products = null;
    private Context context = null;
    private OnItemBtnClickListener mListener;

    public ProductAdapter(@NonNull Context context) {
        this.context = context;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void setOnItemBtnClickListener(OnItemBtnClickListener onItemClickListener) {
        mListener = onItemClickListener;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_product, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        Product product =products.get(position);
        ProductViewHolder pvh = (ProductViewHolder) holder;
        pvh.productNameTv.setText(product.getName());
        pvh.priceTv.setText("￥"+product.getPrice());
        pvh.describeTv.setText(product.getDescribe());
        pvh.position = position;
    }

    @Override
    public int getItemCount() {
        if (products != null && products.size() > 0) {
            return products.size();
        } else {
            return 0;
        }
    }


    public interface OnItemBtnClickListener {
        void onAddShoppingCartBtnClick(int position);
        void onBuyBtnClick(int position);
    }

    private class ProductViewHolder extends RecyclerView.ViewHolder {
        TextView productNameTv;
        TextView priceTv;
        TextView describeTv;
        Button addShoppingCartBtn;
        Button buyBtn;
        Product product;
        int position;
        public ProductViewHolder(View view) {
            super(view);
            productNameTv = view.findViewById(R.id.product_name_tv);
            priceTv = view.findViewById(R.id.price_tv);
            describeTv = view.findViewById(R.id.describe_tv);
            addShoppingCartBtn = view.findViewById(R.id.add_shopping_cart_btn);
            buyBtn = view.findViewById(R.id.buy_btn);
            addShoppingCartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onAddShoppingCartBtnClick(position);
                }
            });
            buyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onBuyBtnClick(position);
                }
            });
        }
    }
}
