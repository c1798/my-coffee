package com.app.mycoffee2_1.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mycoffee2_1.R;
import com.app.mycoffee2_1.api.Util;
import com.app.mycoffee2_1.model.ShoppingCart;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.List;

public class ShoppingCartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ShoppingCart> shoppingCarts;
    private Context context = null;
    private OnItemBtnClickListener mListener;

    public ShoppingCartAdapter(@NonNull Context context, OnItemBtnClickListener listener) {
        this.context = context;
        this.mListener = listener;
    }

    public void setShoppingCarts(List<ShoppingCart> shoppingCarts) {
        this.shoppingCarts = shoppingCarts;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_shopping_cart, parent, false);
        return new ShoppingCartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        ShoppingCart shoppingCart = shoppingCarts.get(position);
        ShoppingCartViewHolder svh = (ShoppingCartViewHolder) holder;
        svh.productNameTv.setText(shoppingCart.getProductName());
        svh.describeTv.setText(shoppingCart.getProductDesc());
        if (shoppingCart.getNum()!=0){
            int num = shoppingCart.getNum();
            svh.numTv.setText(""+num);
            svh.priceTv.setText("￥"+ Util.multiply(shoppingCart.getProductPrice(), shoppingCart.getNum()));
        }
        if (shoppingCart.getIsSelected()){
            svh.selectCd.setChecked(true);
        }

        svh.selectCd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    shoppingCart.setIsSelected(true);
                    svh.selectCd.setChecked(true);
                }else {
                    shoppingCart.setIsSelected(false);
                    svh.selectCd.setChecked(false);
                }
                mListener.onCheckBtnClick(position);
            }
        });
        svh.difBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int newNum = 1;
                int oldNum = Integer.parseInt(svh.numTv.getText().toString());
                if (oldNum == 1){
                    newNum = 1;
                }else {
                    newNum = oldNum - 1;
                }
                svh.numTv.setText(""+newNum);
                BigDecimal price = Util.multiply(shoppingCart.getProductPrice(),newNum);
                svh.priceTv.setText("￥"+price);
                mListener.onMinusBtnClick(position);
            }
        });
        svh.addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int newNum = 1;
                int oldNum = Integer.parseInt(svh.numTv.getText().toString());
                newNum = oldNum + 1;
                svh.numTv.setText(""+newNum);
                BigDecimal price = Util.multiply(shoppingCart.getProductPrice(),newNum);
                svh.priceTv.setText("￥"+price);
                mListener.onAddBtnClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (shoppingCarts != null && shoppingCarts.size() > 0) {
            return shoppingCarts.size();
        } else {
            return 0;
        }
    }

    public interface OnItemBtnClickListener {
        void onCheckBtnClick(int position);

        void onMinusBtnClick(int position);

        void onAddBtnClick(int position);
    }

    private class ShoppingCartViewHolder extends RecyclerView.ViewHolder {
        CheckBox selectCd;
        TextView productNameTv;
        TextView describeTv;
        TextView priceTv;
        TextView numTv;
        ImageView difBtn;
        ImageView addBtn;
        public ShoppingCartViewHolder(View view) {
            super(view);
            selectCd = view.findViewById(R.id.sel_rb);
            productNameTv = view.findViewById(R.id.product_name_tv);
            describeTv = view.findViewById(R.id.describe_tv);
            priceTv = view.findViewById(R.id.product_price_tv);
            numTv = view.findViewById(R.id.shopping_cart_num_tv);
            difBtn = view.findViewById(R.id.dif_btn);
            addBtn = view.findViewById(R.id.add_btn);
        }
    }
}