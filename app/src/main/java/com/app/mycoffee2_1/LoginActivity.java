package com.app.mycoffee2_1;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.mycoffee2_1.api.Api;
import com.app.mycoffee2_1.api.ApiConfig;
import com.app.mycoffee2_1.api.CoffeeCallback;
import com.app.mycoffee2_1.manager.LoginInfoManager;
import com.app.mycoffee2_1.model.Result;
import com.app.mycoffee2_1.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener{

    EditText et_username;
    EditText et_password;
    Button bt_login;
    Button bt_register;

    @Override
    protected int initLayout() {
        return R.layout.login_layout;
    }

    @Override
    protected void initView() {
        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);
        bt_register = findViewById(R.id.btn_register);
        bt_login = findViewById(R.id.btn_login);
    }

    @Override
    protected void initData() {
        bt_login.setOnClickListener(this);
        bt_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()){
            case R.id.btn_login:
                String username = et_username.getText().toString().trim();
                String password = et_password.getText().toString().trim();
                if (username.isEmpty()||password.isEmpty()){
                    showToast("用户名和密码不能为空");
                }else {
                    FormBody formBody = new FormBody.Builder()
                            .add("username", username)
                            .add("password", password)
                            .build();
                    login(formBody);
                }
                break;
            case R.id.btn_register:
                intent.setClass(this,RegisterActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void login(FormBody formBody) {
        Api.config(ApiConfig.LOGIN, formBody).postRequest(new CoffeeCallback() {
            @Override
            public void onSuccess(String res) {
                Gson gson = new Gson();
                Result<User> result = gson.fromJson(res, new TypeToken<Result<User>>(){}.getType());
                if (result.getResultCode()==200){
                    User user = result.getData();
                    LoginInfoManager.getInstance().saveUserInfo(getApplicationContext(), user);
                    navigateToWithFlag(MainActivity.class,Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                }else {
                    et_username.setText("");
                    et_password.setText("");
                }
                System.out.println(result.getMessage());
            }

            @Override
            public void onFailure(Exception e) {
                System.out.println(e);
                showToastSync("请求失败");
            }
        });
    }
}