package com.app.mycoffee2_1.api;

public interface CoffeeCallback {

    void onSuccess(String res);

    void onFailure(Exception e);
}
