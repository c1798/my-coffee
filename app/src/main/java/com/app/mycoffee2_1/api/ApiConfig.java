package com.app.mycoffee2_1.api;

public class ApiConfig {
    public static final int PAGE_SIZE = 5;
    public static final String SP_USER = "sp_user";
    public static final String BASE_URl = "http://121.4.39.22:8080";
    /**
     * 登录
     **/
    public static final String LOGIN = "/user/login";
    /**
     * 注册
     */
    public static final String REGISTER = "/user/register";
    /**
     * 用户更新
     */
    public static final String USER_UPDATE = "/user/update";
    /**
     * 所有商品列表
     */
    public static final String PRODUCT_LIST_ALL = "/product/getAllProducts";
    /**
     * 用户订单列表列表
     **/
    public static final String ORDERS_LIST_BY_USER = "/order/getOrdersByUserId";

    /**
     * 用户购物车列表
     **/
    public static final String SHOPPING_CART_LIST_BY_USER = "/shoppingCart/getShoppingCartByUserId";
    /**
     * 添加购物车
     */
    public static final String SHOPPING_CART_ADD = "/shoppingCart/add";
    /**
     * 添加购物车
     */
    public static final String ORDER_ADD = "/order/addOrder";

}
