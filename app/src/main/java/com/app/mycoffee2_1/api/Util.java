package com.app.mycoffee2_1.api;

import android.content.Context;
import android.content.Intent;

import java.math.BigDecimal;

public class Util {
    public static BigDecimal multiply(BigDecimal arg1, Integer arg2){
        return arg1.multiply(new BigDecimal(arg2.toString()));
    }
}