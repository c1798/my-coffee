package com.app.mycoffee2_1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.mycoffee2_1.api.Api;
import com.app.mycoffee2_1.api.ApiConfig;
import com.app.mycoffee2_1.api.CoffeeCallback;
import com.app.mycoffee2_1.manager.LoginInfoManager;
import com.app.mycoffee2_1.model.Product;
import com.app.mycoffee2_1.model.Result;
import com.app.mycoffee2_1.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {
    EditText et_username;
    EditText et_password;
    EditText et_phone;
    EditText et_address;
    EditText et_nickname;
    Button bt_reg;
    Button bt_reset;

    @Override
    protected int initLayout() {
        return R.layout.register_layout;
    }

    @Override
    protected void initView() {
        et_username = findViewById(R.id.ec_username_r);
        et_password = findViewById(R.id.et_password_r);
        et_phone = findViewById(R.id.et_phone_r);
        et_nickname = findViewById(R.id.et_nickname_r);
        et_address = findViewById(R.id.et_address_r);
        bt_reg = findViewById(R.id.btn_reg);
        bt_reset = findViewById(R.id.btn_reset);
    }

    @Override
    protected void initData() {
        bt_reg.setOnClickListener(this);
        bt_reset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_reg:
                String username = et_username.getText().toString().trim();
                String password = et_password.getText().toString().trim();
                String nickname = et_nickname.getText().toString().trim();
                String address = et_address.getText().toString().trim();
                String phone = et_phone.getText().toString().trim();
                if (username.isEmpty()||password.isEmpty()||nickname.isEmpty()||address.isEmpty()||phone.isEmpty()){
                    showToast("不能为空");
                } else {
                    FormBody formBody = new FormBody.Builder()
                            .add("username", username)
                            .add("password", password)
                            .add("nickname", nickname)
                            .add("address", address)
                            .add("phone", phone)
                            .build();
                    register(formBody);
                }
                break;
            case R.id.btn_reset:
                et_username.setText("");
                et_password.setText("");
                et_phone.setText("");
                et_nickname.setText("");
                et_address.setText("");
                break;
        }
    }

    private void register(FormBody formBody) {
        Api.config(ApiConfig.REGISTER, formBody).postRequest(new CoffeeCallback() {
            @Override
            public void onSuccess(String res) {
                Gson gson = new Gson();
                Result<User> result = gson.fromJson(res, new TypeToken<Result<User>>(){}.getType());
                if (result.getResultCode()==200){
                    navigateToWithFlag(LoginActivity.class,Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                }else {
                    et_username.setText("");
                    et_password.setText("");
                    et_phone.setText("");
                    et_nickname.setText("");
                    et_address.setText("");
                }
                System.out.println(result.getMessage());
            }

            @Override
            public void onFailure(Exception e) {
                System.out.println(e);
                showToastSync("请求失败");
            }
        });
    }


}
