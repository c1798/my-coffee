package com.app.mycoffee2_1.data;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.app.mycoffee2_1.model.Order;
import com.app.mycoffee2_1.model.Product;
import com.app.mycoffee2_1.model.ShoppingCart;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.ContentValues.TAG;

public  class TestData {
    public static List<Product> getAllProducts(){
        List<Product> products = new ArrayList<>();
        products.add(new Product(1L,"摩卡", new BigDecimal("17"),"浓缩的咖啡与牛奶彼此融合"));
        products.add(new Product(2L,"拿铁", new BigDecimal("21"),"经典意式奶咖，口感很圆润"));
        products.add(new Product(3L,"卡布奇诺", new BigDecimal("24"),"定制拉花，展示属于你的美"));
        products.add(new Product(4L,"香草拿铁", new BigDecimal("17"),"拿铁中融入了清新香草风味"));
        products.add(new Product(5L,"焦糖拿铁", new BigDecimal("19"),"拿铁中融入了醇香焦糖风味"));
        products.add(new Product(6L,"标准美式", new BigDecimal("13"),"意式浓缩与水的黄金相配比"));
        products.add(new Product(7L,"加浓美式", new BigDecimal("16"),"比标准美式口感更醇香浓郁"));
        products.add(new Product(8L,"陨石拿铁", new BigDecimal("29"),"独特的黑糖风味拿铁香滑嫩"));
        products.add(new Product(9L,"榛果拿铁", new BigDecimal("27"),"香甜榛果风味，加咖啡牛奶"));
        return products;
    }
//    public static List<ShoppingCart> getAllShoppingCarts(){
//        List<ShoppingCart> shoppingCarts = new ArrayList<>();
//        shoppingCarts.add(new ShoppingCart(1,1,1,1,new BigDecimal("17"), true,"摩卡","浓缩的咖啡与牛奶彼此融合"));
//        shoppingCarts.add(new ShoppingCart(2,1,2,1,new BigDecimal("21"), true,"拿铁","经典意式奶咖，口感很圆润"));
//        return shoppingCarts;
//    }
//    public static List<Order> getAllOrders(){
//        List<Order> orders = new ArrayList<>();
//        orders.add(new Order(BigInteger.valueOf(1), 1111111L, BigInteger.valueOf(1), BigInteger.valueOf(1), 2, 0,"2021-03-21 11:23:21", null, BigDecimal.valueOf(34)));
//        orders.add(new Order(BigInteger.valueOf(2), 1111112L, BigInteger.valueOf(1), BigInteger.valueOf(2), 1, 1,"2021-03-21 11:23:21", null, BigDecimal.valueOf(34)));
//        return orders;
//    }

    public static Product getProductByProductId(BigInteger productId) {
        for (Product p:getAllProducts()) {
            if (p.getId().equals(productId)){
                return p;
            }
        }
        return new Product();
    }
}
